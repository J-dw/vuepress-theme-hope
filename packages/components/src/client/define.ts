import type { ComponentLocaleConfig } from "../shared";

declare const COMPONENT_LOCALES: ComponentLocaleConfig;

export const componentLocales = COMPONENT_LOCALES;
