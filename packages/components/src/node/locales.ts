import type { ComponentLocaleConfig } from "../shared";

export const componentLocales: ComponentLocaleConfig = {
  "/en/": {
    backToTop: "Back to top",
    openInNewWindow: "Open in new window",
    pagination: {
      prev: "Prev",
      next: "Next",
      navigate: "Jump to",
      button: "Go",
      errorText: "Please enter a number between 1 and $page !",
    },
  },

  "/zh/": {
    backToTop: "返回顶部",
    openInNewWindow: "在新窗口中打开",
    pagination: {
      prev: "上一页",
      next: "下一页",
      navigate: "跳转到",
      button: "前往",
      errorText: "请输入 1 到 $page 之前的页码！",
    },
  },

  "/tw/": {
    backToTop: "返回頂部",
    openInNewWindow: "在新窗口中打開",
    pagination: {
      prev: "上一頁",
      next: "下一頁",
      navigate: "跳轉到",
      button: "前往",
      errorText: "請輸入 1 到 $page 之前的頁碼！",
    },
  },

  "/de/": {
    backToTop: "Zurück nach oben.",
    openInNewWindow: "In einem neuen Fenster öffnen",
    pagination: {
      prev: "Vorheriges",
      next: "Nächstes",
      navigate: "Springe zu",
      button: "Los",
      errorText: "Bitte gib eine Nummer zwischen 1 und $page ein!",
    },
  },

  "/vi/": {
    backToTop: "Trở lại đầu trang",
    openInNewWindow: "Mở trong cửa sổ mới",
    pagination: {
      prev: "Bài kế",
      next: "Bài trước",
      navigate: "Đi đến",
      button: "Đi",
      errorText: "Xin hãy nhập 1 số từ 1 đến $page !",
    },
  },

  "/uk/": {
    backToTop: "Повернутися до початку",
    openInNewWindow: "Open in new window",
    pagination: {
      prev: "Попередня",
      next: "Далі",
      navigate: "Перейти до",
      button: "Перейти",
      errorText: "Будь ласка, введіть число від 1 до $page !",
    },
  },

  "/ru/": {
    backToTop: "Вернуться к началу",
    openInNewWindow: "Open in new window",
    pagination: {
      prev: "Предыдущая",
      next: "Далее",
      navigate: "Перейти к",
      button: "Перейти",
      errorText: "Пожалуйста, введите число от 1 до $page !",
    },
  },

  "/br/": {
    backToTop: "Volta ao topo",
    openInNewWindow: "Open in new window",
    pagination: {
      prev: "Anterior",
      next: "Próximo",
      navigate: "Pular para",
      button: "Ir",
      errorText: "Por favor, digite um número entre 1 e $page !",
    },
  },
};
