import type { MarkdownEnhanceLocaleConfig } from "../shared";

export const markdownEnhanceLocales: MarkdownEnhanceLocaleConfig = {
  "/en/": {
    info: "Info",
    tip: "Tips",
    warning: "Note",
    danger: "Warning",
    details: "Details",
  },

  "/zh/": {
    info: "相关信息",
    tip: "提示",
    warning: "注意",
    danger: "相关信息",
    details: "详情",
  },

  "/tw/": {
    info: "相關信息",
    tip: "提示",
    warning: "注意",
    danger: "相关信息",
    details: "詳情",
  },

  "/de/": {
    info: "Information",
    tip: "Tips",
    warning: "Notiz",
    danger: "Warnung",
    details: "Details",
  },

  "/vi/": {
    info: "Thông tin",
    tip: "Tips",
    warning: "Lưu ý",
    danger: "Cẩn thận",
    details: "Chi tiết",
  },

  "/uk/": {
    info: "Інформація",
    tip: "Поради",
    warning: "Примітка",
    danger: "Увага",
    details: "Деталь",
  },

  "/ru/": {
    info: "Информация",
    tip: "Подсказки",
    warning: "Примечание",
    danger: "Предупреждение",
    details: "Деталь",
  },

  "/br/": {
    info: "Informativo",
    tip: "Dicas",
    warning: "Avisos",
    danger: "Cuidado",
    details: "Detalhe",
  },
};
